
#!/usr/bin/env perl

use 5.14.0;
use POSIX;

sub version_up_file {

  my $name = shift @_;
  my $vers = shift @_;
  my $type = shift @_;
  my @lns;

  unless (defined $type) {
    $type = "patch"
  }

  open my $file, "<", $name;

  while (<$file>) {
     if (/$vers\s+/) {
       if ($type eq "major") {
         s/(\d+)\.\d+\.\d+/join('.', $1 + 1, '0', '0')/e
       } elsif ($type eq "minor") {
         s/(\d+)\.(\d+)\.\d+/join('.', $1, $2 + 1, '0')/e
       } elsif ($type eq "patch") {
         s/(\d+)\.(\d+)\.(\d+)/join('.', $1, $2, $3+1)/e
       }
     }
     push @lns, $_
  }

  close $file;

  open $file, ">", $name;

  for (@lns) {
     print { $file } $_;
  }

  close $file;
}

my $type = $ARGV[0];

version_up_file "_oasis", "Version:", $type;
