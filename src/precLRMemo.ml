
open Core.Std

type key = {
  lrm_nt   : string;
  lrm_spos : int
}

type 'a value = {
  mutable lrm_isrec : bool;
  mutable lrm_epos  : ('a PrecT.result * int) option;
  lrm_priority      : int
}

type 'a t = (key, 'a value) Hashtbl.Poly.t

let create () = Hashtbl.Poly.create ()

let insert t k v =
  Hashtbl.add t ~key:k ~data:v |> Pervasives.ignore

let lookup = Hashtbl.find
let remove = Hashtbl.remove
