
open Core.Std

type 'a result =
  | E
  | T of string
  | NT of 'a

type 'a results = 'a result array

let of_list = Array.of_list
let of_list_rev = Array.of_list_rev

let nt_value = function NT v -> v | _ -> invalid_arg "nt_value"
let t_value = function T v -> v | _ -> invalid_arg "t_value"
let e_value = function E -> true | _ -> false

let nt_at a n = nt_value a.(n)
let t_at a n = t_value a.(n)
let e_at a n = e_value a.(n)
