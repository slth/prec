
open Core.Std
open Option

open PrecT
open PrecLRMemo

type t =
  [ `E                     (* Epsilon *)
  | `T of string           (* Terminal *)
  | `NT of string          (* Non-terminal *)
  | `NTp of string * int ] (* Non-terminal with given priority *)


type 'a production = string * (t list * ('a PrecT.results -> 'a)) list
type 'a grammar = string * 'a production list

let default_prio = 1

let rec iter_if_none ~f = function
  | [] -> None
  | x :: xs ->
      match f x with
        | None -> iter_if_none ~f xs
        | v -> v

let rec apply_production lrm gram p input pos =
  let rec aux rets pos = function
    | [] -> Some (PrecT.of_list_rev rets, pos)
    | x :: xs ->
        match x with
          | `E ->
              aux (E :: rets) pos xs
          | `T t ->
              match_terminal t input pos >>= fun (ret, pos') ->
              aux (ret :: rets) pos' xs
          | `NT nt ->
              match_non_terminal lrm gram nt default_prio input pos >>= fun (ret, pos') ->
              aux (ret :: rets) pos' xs
          | `NTp (nt, p) ->
              match_non_terminal lrm gram nt p input pos >>= fun (ret, pos') ->
              aux (ret :: rets) pos' xs
  in aux [] pos p

and match_terminal t input pos =
  let t_len = String.length t in
  let i_len = String.length input in
  let pos' = pos + t_len in
  if pos' <= i_len then
    if String.is_prefix ~prefix:t (String.sub ~pos ~len:t_len input) then begin
      Some (T t, pos')
    end else
      None
  else
    None

and match_non_terminal lrm gram nt p input pos =
  let prod = List.Assoc.find_exn gram nt in
  let lr_key = { lrm_spos = pos; lrm_nt = nt } in
  match lookup lrm lr_key with
    | None ->
        let lr_data = { lrm_epos = None; lrm_isrec = false; lrm_priority = p } in
        insert lrm lr_key lr_data;
        let rec resolve last_pos =
          match
            iter_if_none ~f:(fun (p, f) ->
              apply_production lrm gram p input pos >>= fun (ret, pos) ->
              return (f ret, pos)
            ) prod
          with
            | Some (ret', pos') when pos' > last_pos ->
                let ret'' = Some (NT ret', pos') in
                if lr_data.lrm_isrec then begin
                  lr_data.lrm_epos <- ret''; 
                  resolve pos'
                end else begin
                  remove lrm lr_key;
                  ret''
                end
            | None when not lr_data.lrm_isrec ->
                remove lrm lr_key;
                None
            | None ->
                remove lrm lr_key;
                if pos <> last_pos then
                  lr_data.lrm_epos
                else
                  None
            | Some _ ->
                remove lrm lr_key;
                lr_data.lrm_epos
        in resolve pos
    | Some lr_data ->
        lr_data.lrm_isrec <- true;
        if p >= lr_data.lrm_priority then
          lr_data.lrm_epos
        else
          None

let parse (nt, gram) input =
  let lrm = create () in
  match_non_terminal lrm gram nt default_prio input 0 >>= fun (v, _) ->
  return (PrecT.nt_value v)
