
type t =
  [ `E                     (* Epsilon *)
  | `T of string           (* Terminal *)
  | `NT of string          (* Non-terminal *)
  | `NTp of string * int ] (* Non-terminal with given priority *)

type 'a production = string * (t list * ('a PrecT.results -> 'a)) list
type 'a grammar = string * 'a production list

val default_prio : int

val parse : 'a grammar -> string -> 'a option
