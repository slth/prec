
type 'a result =
  | E
  | T of string
  | NT of 'a

type 'a results

val of_list     : 'a result list -> 'a results
val of_list_rev : 'a result list -> 'a results

val nt_at : 'a results -> int -> 'a
val t_at  : 'a results -> int -> string
val e_at  : 'a results -> int -> bool

val nt_value : 'a result -> 'a
val t_value  : 'a result -> string
val e_value  : 'a result -> bool
