
open Core.Std

let rec ( ** ) n m = if m = 0 then 1 else n * n ** (m - 1)

let calc_grammar : int Prec.grammar =
  "E",
  [
    "E", [
      [`NTp ("E", 1); `T "+"; `NTp ("E", 2)], (fun a -> PrecT.(nt_at a 0 + nt_at a 2));
      [`NTp ("E", 1); `T "-"; `NTp ("E", 2)], (fun a -> PrecT.(nt_at a 0 - nt_at a 2));
      [`NTp ("E", 2); `T "*"; `NTp ("E", 3)], (fun a -> PrecT.(nt_at a 0 * nt_at a 2));
      [`NTp ("E", 2); `T "/"; `NTp ("E", 3)], (fun a -> PrecT.(nt_at a 0 / nt_at a 2));
      [`NTp ("E", 4); `T "**"; `NTp ("E", 3)], (fun a -> PrecT.(nt_at a 0 ** nt_at a 2));
      [`T "-"; `NTp ("E", 4)], (fun a -> - (PrecT.nt_at a 1));
      [`T "("; `NTp ("E", 1); `T ")"], (fun a -> PrecT.nt_at a 1);
      [`NT "N"], (fun a -> PrecT.nt_at a 0)
    ];
    "N", [
      [`T "0"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "1"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "2"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "3"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "4"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "5"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "6"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "7"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "8"], (fun a -> Int.of_string (PrecT.t_at a 0));
      [`T "9"], (fun a -> Int.of_string (PrecT.t_at a 0))
    ]
  ]

let calculate = Prec.parse calc_grammar
