
type key = {
  lrm_nt   : string;
  lrm_spos : int
}

type 'a value = {
  mutable lrm_isrec : bool;
  mutable lrm_epos  : ('a PrecT.result * int) option;
  lrm_priority      : int
}

type 'a t

val create : unit -> 'a t

val insert : 'a t -> key -> 'a value -> unit
val lookup : 'a t -> key -> 'a value option
val remove : 'a t -> key -> unit
