
#!/bin/sh

if [ $# -ne 1 ]; then
  echo "$0: <version information>"
  exit -1
fi

perl semver-up.pl $1
oasis setup

git add .
git commit

git push -u origin --all

./configure
make
