
PREC: PEG Recogniser
====================
A simple PEG recogniser with support for left-recursive grammars following the
semantics outlined in [1](http://arxiv.org/abs/1207.0443).

References
----------
[1] Left Recursion in Parsing Expression Grammars, 2012, Sérgio Medeiros, Fabio Mascarenhas, Roberto Ierusalimschy
